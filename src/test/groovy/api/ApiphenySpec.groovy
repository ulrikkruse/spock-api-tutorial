package api

import util.CommonUtil
import util.CommonConstants
import spock.lang.Unroll
import com.fasterxml.jackson.databind.JsonNode

@Unroll
class ApiphenySpec extends CommonUtil {
    void "genderize (#name)"(){
        given:
        String url = CommonConstants.GENDERIZE_BASE_URL + name

        when: "you call API with a given name"
        JsonNode rootNode = getFraService(url)

        then: "the correct gender..."
        rootNode["gender"] =~ gender

        and: "...  and probability are returned"
        rootNode["probability"].toString() == probability

        where:
        name              || gender   || probability
        "Ulrik"           || "male"   || "1.0"
        "Tanja"           || "female" || "0.99"
        "Kim"             || "female" || "0.7"
        "hkjhljkhjkbnmnl" || "null"   || "0.0"
    }

    void "agify (#name)"(){
        given:
        String url = CommonConstants.AGIFY_BASE_URL + name

        when: "you call API with a given name"
        JsonNode rootNode = getFraService(url)

        then: "the correct age is returned"
        rootNode["age"].toString() == age

        where:
        name              || age
        "Ulrik"           || "57"
        "hkjhljkhjkbnmnl" || "null"
    }
}
