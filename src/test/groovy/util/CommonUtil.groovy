package util

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.web.reactive.function.client.WebClient
import spock.lang.Specification

class CommonUtil extends Specification{
    ObjectMapper mapper = new ObjectMapper()
    WebClient client = WebClient.create()

    JsonNode getFraService(String url){
        String responseBody

        def responseSpec = client.get()
                .uri(url)
                .retrieve()

        responseBody = responseSpec.bodyToMono(String.class).block()

        JsonNode rootNode = mapper.readTree(responseBody)

        return rootNode
    }
}
