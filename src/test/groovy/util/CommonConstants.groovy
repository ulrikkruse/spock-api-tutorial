package util

class CommonConstants {
    static final String GENDERIZE_BASE_URL = "https://api.genderize.io?name="
    static final String AGIFY_BASE_URL = "https://api.agify.io?name="
}
