# Tutorial on how to use spock to test API's

The API's used in this tutorial are all from this great site:
https://apipheny.io/free-api/

# Setup
Install:
- Git
- Gradle
- Java
- Groovy

Spring Boot 3.0 requires Java 17.

IDEA:
- Go to the settings --> 
    Build, Execution, Deployment --> 
    Build Tools --> Gradle. 
    Click on your gradle project under 'Gradle Projects'.
    Choose your Gradle JVM for the project.
- Make sure SDK is set correctly in Project settings.

# Disclaimer
Only verified in IntelliJ IDEA on Mac.